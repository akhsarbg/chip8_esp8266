#include "osapi.h"

#include "util.h"
#include "cpu.h"

char keymap[16][2] = {
    {1,    1},
    {1,    2},
    {1,    3},
    {1,   0xc},
    {1,       4},
    {1,       5},
    {1,       6},
    {1,      0xd},
    {1,       7},
    {1,       8},
    {1,       9},
    {1,      0xe},
    {1,      0xa},
    {1,       0},
    {1,      0xb},
    {1,      0xf},
};

void util_rand_init(){
    //srand(time(NULL));   // should only be called once
}

int util_rand(){
    //static int i = 0;
    return os_random() % 0xFF;
}

/*
  Used on machines with more keys that chip8

  Fuction is depricated
*/
bool is_valid_key_code(int key){
    int i;
    for(i=0; i<0xF; i++)
        if(key == keymap[i][0])
            return true;
    return false;
}

void set_pixel(int x, int y){
    uint16_t index = (y*VIDEO_WEIGHT + x) / BYTE_LENGTH;
    uint16_t offset = x % BYTE_LENGTH;
    uint16_t bit = (BYTE_LENGTH-1) - offset;

    video_memory[index] ^= ( 1 << bit );

    // No need to perform this is V[0xF]
    // is true from earlier
    if(!V[0xF])
        V[0xF] |=  !(video_memory[index] >> bit & 1);
}

bool is_pixel_set(int x, int y){
    uint8_t b = -1;
    uint16_t index = (y*VIDEO_WEIGHT + x) / BYTE_LENGTH;
    uint16_t offset = x % BYTE_LENGTH;
    uint16_t bit = (BYTE_LENGTH-1) - offset;

    b = (video_memory[index] >> bit) & 1;
    return b;
}

double pow(double x, double y){
    int sum = 1;
    while(y--)
        sum *= x;
    return sum;
}

/* Binary-Coded Decimal */
void bcd(int dec){
    int i, sum = 0;

    for(i=0; i<BYTE_LENGTH; i++){
        sum += (dec >> i & 1) * pow(2, i);
    }

    ram_memory[IR+0] = sum % 1000 / 100;
    ram_memory[IR+1] = sum % 100 / 10;
    ram_memory[IR+2] = sum % 10 / 1;
}

int key_to_map(int key){
    int i;
    for(i=0; i<0xF; i++){
        if(key == keymap[i][1]) return keymap[i][0];
    }
    return -1;
}

int map_to_key(int map){
    int i;
    for(i=0; i<0xF; i++){
        if(map == keymap[i][0]) return keymap[i][1];
    }
    return -1;
}

void print_debug_info(){
    int i;
    os_printf("IR: %X\n", IR);
    os_printf("PC: %X\n", PC);
    os_printf("Delay: %X\n", delay_timer);
    os_printf("Sound: %X\n\n", sound_timer);
    for(i=0; i<0xF; i++){
        os_printf("V[%x]: %X\n", i, V[i]);
    }
    stack_print();
}

void print_debug_and_exit(){
    print_debug_info();
    //exit(1);
}

void hexDump (char *desc, void *addr, int len) {
    int i;
    unsigned char buff[17];
    unsigned char *pc = (unsigned char*)addr;

    // Output description if given.
    if (desc != NULL)
        os_printf ("%s:\n", desc);

    if (len == 0) {
        os_printf("  ZERO LENGTH\n");
        return;
    }
    if (len < 0) {
        os_printf("  NEGATIVE LENGTH: %i\n",len);
        return;
    }

    // Process every byte in the data.
    for (i = 0; i < len; i++) {
        // Multiple of 16 means new line (with line offset).

        if ((i % 16) == 0) {
            // Just don't print ASCII for the zeroth line.
            if (i != 0)
                os_printf ("  %s\n", buff);

            // Output the offset.
            os_printf ("  %04x ", i);
        }

        // Now the hex code for the specific character.
        os_printf (" %02x", pc[i]);

        // And store a printable ASCII character for later.
        if ((pc[i] < 0x20) || (pc[i] > 0x7e))
            buff[i % 16] = '.';
        else
            buff[i % 16] = pc[i];
        buff[(i % 16) + 1] = '\0';
    }

    // Pad out last line if not exactly 16 characters.
    while ((i % 16) != 0) {
        os_printf ("   ");
        i++;
    }

    // And print the final ASCII bit.
    os_printf ("  %s\n", buff);
}
