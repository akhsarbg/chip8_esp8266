//#include <SFML/Audio.h>
//#include <SFML/Window/Keyboard.h>
//#include <SFML/Graphics.h>
#include "osapi.h"

#include "ssd1306.h"
#include "chip8.h"
#include "cpu.h"
#include "io.h"
#include "util.h"
#include "brzo_i2c.h"


int io_video_init(){
    brzo_i2c_setup(200);
    init_display();
    return 0;
}

int io_sound_init(){
    //Currently no sound available
    return 0;
}

int io_keyboard_init(){
    return 0;
}


void io_video_draw(uint8_t x, uint8_t y, uint8_t n){
    int bit, offset = 0;
    uint8_t current;
    /* Update video memory */
    //printf("draw(%d, %d, %d)\n",x,y,n);
    for(offset = 0; offset < n; offset++){
        current = ram_memory[IR + offset];
        //printf("Current: %x\n", current);
        for(bit=0; bit < BYTE_LENGTH; bit++){
            if( ((current >> (BYTE_LENGTH - 1 - bit)) & 0x1) == 1){
                set_pixel((x+bit) % VIDEO_WEIGHT,
                          (y+offset) % VIDEO_HEIGHT);
            }
        }
    }
    /* Draw sprite */
    // TODO: Only redraw needed area
    int x0 = x*2;
    int x1 = (x0 + 8*2) >= WIDTH ? WIDTH-1 : (x0 + 8*2);
    int y0 = y*2;
    int y1 = (y0 + n*2) >= HEIGHT ? HEIGHT-1: (y0 + n*2);

    /* Update the window */
    io_video_render();
    render(x0, x1, y0, y1);
    //print_debug_info();
}

void io_video_clear(){
    /* Clear video momory */
    os_memset(video_memory, 0, VIDEO_LENGTH-1);

    /* Update the window */
    io_video_reflesh();
}

void io_video_render(){
    clear();

    for(int y=0; y < 32; y++){
        for(int x=0; x < 64; x++){
            if( is_pixel_set(x, y) ){
                setXY(x*2,   y*2);
                setXY(x*2+1, y*2);
                setXY(x*2,   y*2+1);
                setXY(x*2+1, y*2+1);
            }
        }
    }
}

void io_video_reflesh(){
    io_video_render();
    show();
}

bool io_video_running(){
    return true;
}

void io_sound_play(){}

void io_sound_stop(){}

bool io_key_isPressed(int key){
    key = key_to_map(key);
    return false;
}

int io_key_get(){
    for(;;){
        int code = 1;
        if(map_to_key(code) == -1)
            continue;
        else
            return map_to_key(code);

        os_delay_us(10);
    }
}


void io_shutdown(){
    /* Cleanup resources */
    clear();
    io_video_render();

    // TODO: Implement proper shutdown
}
