#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"
#include "user_interface.h"

#include "chip8.h"
#include "io.h"

int user_init(int argc, char** argv){

    uart_div_modify(0, UART_CLK_FREQ / 115200);

    os_printf("\n");
    os_printf("SDK version:%s\n", system_get_sdk_version());
    os_printf("CHIP8\n");
    
	//init all sybsystems
	chip8_cpu_init();
	chip8_io_init();
	chip8_util_init();
	
	//load game
	chip8_load_rom("maze");
    
    io_video_clear();

	//start main loop
	chip8_run();
}
