#ifndef MAIN_H
#define MAIN_H

#define HEIGHT 64
#define WIDTH 128

#define PAGE_LENGTH 8
#define PAGES HEIGHT / PAGE_LENGTH

#define SET_CONTRAST        0x81
#define SET_ENTIRE_ON       0xA4
#define SET_NORM_INV        0xA6
#define SET_DISP            0xAE
#define SET_MEM_ADDR        0x20
#define SET_COL_ADDR        0x21
#define SET_PAGE_ADDR       0x22
#define SET_DISP_START_LINE 0x40
#define SET_SEG_REMAP       0xA0
#define SET_MUX_RATIO       0xA8
#define SET_COM_OUT_DIR     0xC0
#define SET_DISP_OFFSET     0xD3
#define SET_COM_PIN_CFG     0xDA
#define SET_DISP_CLK_DIV    0xD5
#define SET_PRECHARGE       0xD9
#define SET_VCOM_DESEL      0xDB
#define SET_CHARGE_PUMP     0x8D

void init_display();
void poweroff();
void poweron();
void contrast(int contrast);
void invert(bool invert);
void render(int x0, int x1, int y0, int y1);
void show();
void clear();
void setXY(int x, int y);
void clearXY(int x, int y);

void write_raw(unsigned char* buf, int len);
void write_cmd(unsigned char cmd);
#endif
