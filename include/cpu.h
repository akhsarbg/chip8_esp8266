#ifndef CHIP8_GUI_CPU_H
#define CHIP8_GUI_CPU_H

#define BYTE_LENGTH  8

#define RAM_LENGTH  4096
#define VIDEO_HEIGHT  32
#define VIDEO_WEIGHT  64
#define VIDEO_WIDHT  64
#define VIDEO_LENGTH  (VIDEO_HEIGHT*VIDEO_WEIGHT/8)
#define _START  0x200

//16 x 8-bit registers (V0-Vf)
uint8_t V[0xF+1];
//16-bit index register
uint16_t IR;
//16-bit program counter register
uint16_t PC;
//8-bit delay timer register
uint8_t delay_timer;
//8-bit sound timer register
uint8_t sound_timer;
//64x32 bit frame buffer
uint8_t video_memory[VIDEO_LENGTH];
//4096-bit ram buffer
uint8_t ram_memory[RAM_LENGTH];

uint16_t ticks;

void stack_init();
void stack_print();

void cpu_tick();
void cpu_execute_opcode(uint16_t);

#endif
