#ifndef CHIP8_GUI_H
#define CHIP8_GUI_H

#include <stdint.h>


extern const uint8_t font[80];
extern const size_t font_length;
extern const size_t char_height;
extern const size_t char_size;

uint16_t ticks;

int chip8_io_init();
void chip8_cpu_init();
int chip8_util_init();

int chip8_load_rom(char*);

void chip8_run();
void chip8_shutdown();
#endif
