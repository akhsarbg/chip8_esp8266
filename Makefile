# Makefile for ESP8266 development
# Tom Trebisky  12-26-2015

# tjt - be verbose
V = 1

V ?= $(VERBOSE)
ifeq ("$(V)","1")
Q :=
vecho := @true
else
Q := @
vecho := @echo
endif

# The new python job
ESPTOOL		= esptool.py
BAUD		= 921600
PORT		= /dev/ttyUSB0

# base directory of the ESP8266 SDK package, absolute
SDK_BASE	= /home/axcap/src/esp-open-sdk

# Base directory for the compiler
SDK_BIN = $(SDK_BASE)/xtensa-lx106-elf/bin

# select which tools to use as compiler, librarian and linker
CC		= xtensa-lx106-elf-gcc
AR		= xtensa-lx106-elf-ar
LD		= xtensa-lx106-elf-gcc

# various paths from the SDK used in this project
SDK_LIBDIR	= $(SDK_BASE)/sdk/lib
SDK_INCDIR	= $(SDK_BASE)/sdk/include

SDK_DRIVER_INCDIR =  $(SDK_BASE)/sdk/driver_lib/include/
# linker script used for the linker step
LD_SCRIPT	= $(SDK_BASE)/sdk/ld/eagle.app.v6.ld

# contents of /home/user/ESP8266/esp-open-sdk/esp_iot_sdk_v1.4.0/lib
# libat.a  libcrypto.a  libespnow.a  libjson.a  liblwip_536.a  liblwip.a  libmain.a  libmesh.a  libnet80211.a  libphy.a  libpp.a  libpwm.a  libsmartconfig.a  libssl.a  libupgrade.a  libwpa.a  libwps.a

# libraries used in this project, mainly provided by the SDK (with 1.4.0)
#LIBS		= c gcc hal pp phy net80211 lwip wpa main
LIBS		= c gcc hal pp phy net80211 lwip wpa main
LIBS		:= $(addprefix -l,$(LIBS))

# compiler includes
#INCLUDES = -I. -I$(SDK_INCDIR) -I$(SDK_DRIVER_INCDIR)
INCLUDES = -I./include/. -I$(SDK_INCDIR)

# compiler flags
CFLAGS		= -std=gnu99 -Os -O2 -g -Wpointer-arith -Wundef -Werror -Wl,-EL -fno-inline-functions -nostdlib -mlongcalls -mtext-section-literals  -D__ets__ -DICACHE_FLASH

# linker flags
LDFLAGS		= -nostdlib -Wl,--no-check-sections -u call_user_start -Wl,-static

.PHONY: all main2 flash clean info

SRC=src
SRCS = \
	$(SRC)/main.c 	 	\
	$(SRC)/chip8.c 	 	\
	$(SRC)/cpu.c 	 	\
	$(SRC)/io.c 	 	\
	$(SRC)/util.c		\
	$(SRC)/brzo_i2c.c	\
	$(SRC)/ssd1306.c

TARGET = main

all: main2

main2 : $(SRCS)
	$(vecho) "CC $<"
	$(Q) $(CC) $(INCLUDES) $(CFLAGS)  -c $(SRCS)
	$(vecho) "LD $@"
	$(Q) $(LD) -L$(SDK_LIBDIR) -T$(LD_SCRIPT) $(LDFLAGS) -Wl,--start-group $(LIBS) *.o -Wl,--end-group -o main
	$(Q) chmod -x main
	$(Q) rm *.o


# This is important -- without the right options it works sometimes,
# but other times screws up.
# Flash options - anything with a 12E module will be dio
FLOPS = -fm dio -fs 16MB

# This loads our code into the flash on the device itself
flash:	$(TARGET)
	$(ESPTOOL) elf2image $(TARGET)
	-$(ESPTOOL) --baud $(BAUD) --port $(PORT) write_flash $(FLOPS) 0x00000 $(TARGET)-0x00000.bin 0x10000 $(TARGET)-0x10000.bin

# This is a good way to verify that the boot loader on the ESP8266 is running
info:
	$(ESPTOOL) -p $(PORT) read_mac
	$(ESPTOOL) -p $(PORT) flash_id

clean:
	$(Q) rm -f main
	$(Q) rm -f *.bin
	$(Q) rm -f *.o
